<?php
/**
 * @file
 * landing_page.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function landing_page_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-landing_page-field_widgets'.
  $field_instances['node-landing_page-field_widgets'] = array(
    'bundle' => 'landing_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_widgets',
    'label' => 'Widgets',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'about' => 'about',
        'banner' => 'banner',
        'countdown' => 'countdown',
        'free_stage_bands' => -1,
        'free_stages' => 'free_stages',
        'headliner' => -1,
        'headliners' => 'headliners',
        'logo_banner' => 'logo_banner',
        'parking' => 'parking',
        'parking_garage' => -1,
        'pricing' => 'pricing',
        'sponsors' => 'sponsors',
        'stage' => -1,
        'tickets' => 'tickets',
      ),
      'bundle_weights' => array(
        'about' => 2,
        'banner' => 3,
        'countdown' => 4,
        'free_stage_bands' => 17,
        'free_stages' => 16,
        'headliner' => 5,
        'headliners' => 6,
        'logo_banner' => 7,
        'parking' => 8,
        'parking_garage' => 9,
        'pricing' => 10,
        'sponsors' => 11,
        'stage' => 25,
        'tickets' => -22,
      ),
      'default_edit_mode' => 'closed',
      'title' => 'Widget',
      'title_multiple' => 'Widgets',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 42,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Widgets');

  return $field_instances;
}
