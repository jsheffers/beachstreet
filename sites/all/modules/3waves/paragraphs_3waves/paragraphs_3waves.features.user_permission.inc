<?php
/**
 * @file
 * paragraphs_3waves.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function paragraphs_3waves_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view paragraph content about'.
  $permissions['view paragraph content about'] = array(
    'name' => 'view paragraph content about',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content banner'.
  $permissions['view paragraph content banner'] = array(
    'name' => 'view paragraph content banner',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content countdown'.
  $permissions['view paragraph content countdown'] = array(
    'name' => 'view paragraph content countdown',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content free_stage_bands'.
  $permissions['view paragraph content free_stage_bands'] = array(
    'name' => 'view paragraph content free_stage_bands',
    'roles' => array(),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content free_stages'.
  $permissions['view paragraph content free_stages'] = array(
    'name' => 'view paragraph content free_stages',
    'roles' => array(),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content headliner'.
  $permissions['view paragraph content headliner'] = array(
    'name' => 'view paragraph content headliner',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content headliners'.
  $permissions['view paragraph content headliners'] = array(
    'name' => 'view paragraph content headliners',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content logo_banner'.
  $permissions['view paragraph content logo_banner'] = array(
    'name' => 'view paragraph content logo_banner',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content parking'.
  $permissions['view paragraph content parking'] = array(
    'name' => 'view paragraph content parking',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content parking_garage'.
  $permissions['view paragraph content parking_garage'] = array(
    'name' => 'view paragraph content parking_garage',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content pricing'.
  $permissions['view paragraph content pricing'] = array(
    'name' => 'view paragraph content pricing',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content sponsors'.
  $permissions['view paragraph content sponsors'] = array(
    'name' => 'view paragraph content sponsors',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content stage'.
  $permissions['view paragraph content stage'] = array(
    'name' => 'view paragraph content stage',
    'roles' => array(),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content tickets'.
  $permissions['view paragraph content tickets'] = array(
    'name' => 'view paragraph content tickets',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
