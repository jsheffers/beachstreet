<?php
/**
 * @file
 * paragraphs_3waves.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function paragraphs_3waves_paragraphs_info() {
  $items = array(
    'about' => array(
      'name' => 'About',
      'bundle' => 'about',
      'locked' => '1',
    ),
    'banner' => array(
      'name' => 'Banner',
      'bundle' => 'banner',
      'locked' => '1',
    ),
    'countdown' => array(
      'name' => 'Count down',
      'bundle' => 'countdown',
      'locked' => '1',
    ),
    'custom_link' => array(
      'name' => 'Custom link',
      'bundle' => 'custom_link',
      'locked' => '1',
    ),
    'free_stage_bands' => array(
      'name' => 'Free stage Bands',
      'bundle' => 'free_stage_bands',
      'locked' => '1',
    ),
    'free_stages' => array(
      'name' => 'Free stages',
      'bundle' => 'free_stages',
      'locked' => '1',
    ),
    'headliner' => array(
      'name' => 'Headliner',
      'bundle' => 'headliner',
      'locked' => '1',
    ),
    'headliners' => array(
      'name' => 'Headliners',
      'bundle' => 'headliners',
      'locked' => '1',
    ),
    'logo_banner' => array(
      'name' => 'Logo banner',
      'bundle' => 'logo_banner',
      'locked' => '1',
    ),
    'parking' => array(
      'name' => 'Parking Info',
      'bundle' => 'parking',
      'locked' => '1',
    ),
    'parking_garage' => array(
      'name' => 'Parking Garage',
      'bundle' => 'parking_garage',
      'locked' => '1',
    ),
    'pricing' => array(
      'name' => 'Pricing',
      'bundle' => 'pricing',
      'locked' => '1',
    ),
    'sponsors' => array(
      'name' => 'Sponsors',
      'bundle' => 'sponsors',
      'locked' => '1',
    ),
    'stage' => array(
      'name' => 'Stage',
      'bundle' => 'stage',
      'locked' => '1',
    ),
    'tickets' => array(
      'name' => 'Tickets',
      'bundle' => 'tickets',
      'locked' => '1',
    ),
  );
  return $items;
}
