<?php
/**
 * @file
 * instagram_feed_3waves.features.inc
 */

/**
 * Implements hook_views_api().
 */
function instagram_feed_3waves_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
