<?php
/**
 * @file
 * instagram_feed_3waves.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function instagram_feed_3waves_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'instagram_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'instagram_social_feed_photos';
  $view->human_name = 'Instagram feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '#LiveOnAtlantic';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Footer: Global: Unfiltered text */
  $handler->display->display_options['footer']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['footer']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom']['content'] = '<p><a href="media/photos" class="c-btn">Photo Gallery</a></p>';
  /* Field: Instagram photo: Standard_resolution */
  $handler->display->display_options['fields']['standard_resolution']['id'] = 'standard_resolution';
  $handler->display->display_options['fields']['standard_resolution']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['standard_resolution']['field'] = 'standard_resolution';
  $handler->display->display_options['fields']['standard_resolution']['label'] = '';
  $handler->display->display_options['fields']['standard_resolution']['exclude'] = TRUE;
  $handler->display->display_options['fields']['standard_resolution']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['standard_resolution']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['standard_resolution']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['standard_resolution']['display_as_link'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="[standard_resolution]">';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Instagram photo: Created_time */
  $handler->display->display_options['sorts']['created_time']['id'] = 'created_time';
  $handler->display->display_options['sorts']['created_time']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['sorts']['created_time']['field'] = 'created_time';
  /* Filter criterion: Instagram photo: Approve */
  $handler->display->display_options['filters']['approve']['id'] = 'approve';
  $handler->display->display_options['filters']['approve']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['filters']['approve']['field'] = 'approve';
  $handler->display->display_options['filters']['approve']['value']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'Instagram feed';
  $translatables['instagram_feed'] = array(
    t('Master'),
    t('#LiveOnAtlantic'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<p><a href="media/photos" class="c-btn">Photo Gallery</a></p>'),
    t('<img src="[standard_resolution]">'),
    t('Block'),
    t('Instagram feed'),
  );
  $export['instagram_feed'] = $view;

  return $export;
}
