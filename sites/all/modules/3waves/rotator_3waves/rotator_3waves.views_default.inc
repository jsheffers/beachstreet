<?php
/**
 * @file
 * rotator_3waves.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function rotator_3waves_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'rotator';
  $view->description = 'Rotator view on the home page';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Rotator';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Rotator';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_rotator_link']['id'] = 'field_rotator_link';
  $handler->display->display_options['fields']['field_rotator_link']['table'] = 'field_data_field_rotator_link';
  $handler->display->display_options['fields']['field_rotator_link']['field'] = 'field_rotator_link';
  $handler->display->display_options['fields']['field_rotator_link']['label'] = '';
  $handler->display->display_options['fields']['field_rotator_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_rotator_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_rotator_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_rotator_link']['type'] = 'link_plain';
  /* Field: Content: Rotator */
  $handler->display->display_options['fields']['field_rotator_image']['id'] = 'field_rotator_image';
  $handler->display->display_options['fields']['field_rotator_image']['table'] = 'field_data_field_rotator_image';
  $handler->display->display_options['fields']['field_rotator_image']['field'] = 'field_rotator_image';
  $handler->display->display_options['fields']['field_rotator_image']['label'] = '';
  $handler->display->display_options['fields']['field_rotator_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_rotator_image']['alter']['path'] = '[field_rotator_link]';
  $handler->display->display_options['fields']['field_rotator_image']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_rotator_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_rotator_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_rotator_image']['settings'] = array(
    'image_style' => 'rotator',
    'image_link' => '',
  );
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'rotator' => 'rotator',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['rotator'] = array(
    t('Master'),
    t('Rotator'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Block'),
  );
  $export['rotator'] = $view;

  return $export;
}
