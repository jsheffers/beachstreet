<?php
/**
 * @file
 * rotator_3waves.features.inc
 */

/**
 * Implements hook_views_api().
 */
function rotator_3waves_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
