<?php
/**
 * @file
 * This file contains no working PHP code; it's here to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Alter the value(s) on the regular filter form based on contextual filter.
 *
 * Allow other modules to change the filling-out of the exposed filter form,
 * based on the value currently set for the contextual filter.
 *
 * @param array $form
 *   The form.
 * @param object $contextual_filter
 *   The contextual filter
 * @param string $value
 *   The contextual filter value, like a space-separated string of numbers
 */
function hook_filter_harmonizer_set_alter(&$form, $contextual_filter, $value) {
  // See plugins/filter_harmonizer_geofield.inc for an example.
}

/**
 * Allow modules to alter the meaning of 'empty' for regular filter values.
 *
 * By default, the number 0 and a sequence of one or more spaces are not
 * considered empty for any filter, but other modules may override this by
 * implementing this hook.
 *
 * @param mixed $value
 *   the value that is to be tested on "emptiness"
 * @param bool $is_empty
 *   set this according to whether $value is considered empty or not
 * @param object $filter_handler
 *   the associated Views exposed filter handler, if needed
 */
function hook_filter_harmonizer_filter_is_empty($value, &$is_empty, $filter_handler) {
}
