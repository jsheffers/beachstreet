<?php
/**
 * @file
 * filter_harmonizer.admin.module
 *
 * Configuration form for global and advanced filter harmonizer settings.
 */

/**
 * Menu callback for admin settings.
 */
function filter_harmonizer_admin_config() {
  $form['filter_harmonizer_settings_global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter Harmonizer global settings'),
  );
  $form['filter_harmonizer_settings_global']['filter_harmonizer_always'] = array(
    '#type' => 'checkbox',
    '#title' => t('After initial page load, ignore <strong>any</strong> <em>contextual</em> filter that also has a companion <em>exposed</em> filter for the same field'),
    '#default_value' => variable_get('filter_harmonizer_always', FALSE),
    '#description' => t("If checked, the equivalent per-field check box on the contextual and exposed filter configuration panels <em>won't</em> appear for any field."),
  );
  $form['filter_harmonizer_settings_global']['filter_harmonizer_fill_exposed'] = array(
    '#type' => 'checkbox',
    '#title' => t('At initial page load auto-fill the exposed filter with the contextual filter value(s) applied to the page'),
    '#default_value' => variable_get('filter_harmonizer_fill_exposed', TRUE),
    '#description' => t('While not affecting the filter behaviour itself, this is useful visual feedback as to what is happening. <br/>May not work on some multi-valued exposed filter forms.'),
  );

  $form['filter_harmonizer_settings_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter Harmonizer advanced settings'),
  );
  $form['filter_harmonizer_settings_advanced']['filter_harmonizer_debug_users'] = array(
    '#type' => 'textfield',
    '#title' => t('Show informational messages during execution'),
    '#default_value' => variable_get('filter_harmonizer_debug_users', ''),
    '#description' => t('Enter a comma-separated list of names of users that should see info messages coming from this module, e.g., for debugging purposes. Use <strong>anon</strong> for the anonymous user.<br/>Debug messages are likely to show incorrect values on View displays with AJAX set to "Yes".'),
  );
  return system_settings_form($form);
}
