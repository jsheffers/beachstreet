<div class="<?php print $classes; ?>"<?php print $attributes; ?> id="freestages">
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>
</div>
