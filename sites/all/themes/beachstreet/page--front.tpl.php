<?php


?>
<div class="header--mobile">
  <div class="header-banner">
    <a href="#" class="mobile-toggle">Menu</a>
  </div>
  <div class="header-menu">
    <?php print render($page['nav']);?>
  </div>
</div>

<div id="header"><div class="section">



	<?php print render($page['topheader']);?>
	<div id="menu">
		<?php print render($page['nav']);?>
	</div>
	<div class="clearfix"></div>

	<div id="mega"></div>
</div></div><!-- /Section & /Header -->

<div id="hider">
	<div style="display:none;"><?php print render($page['content']);?></div>
	<div id="rotator-wrapper">

    <div class="logo_wrap">
  		<div id="shadow"></div>
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
    </div>


		<div id="rotator">
			<?php print render($page['rotator']);?>
		</div>
	</div>
</div>

<!-- <div id="scrollers">
	<img src="/sites/all/themes/beachstreet/images/kites.png" class="kite right" />
	<img src="/sites/all/themes/beachstreet/images/kites.png" class="kite left" />
</div> -->

<div class="site-title__wrap">
  <div class="wrapper">
    <h1 class="site-title">BeachstreetUSA.com is now LiveonAtlantic.com</h1>
    <ul class="social-list">
      <li class="social-list__item">
        <a href="http://www.facebook.com/liveonatlantic" class="social-list--facebook" target="_blank"></a>
      </li>
      <li class="social-list__item">
        <a href="http://www.instagram.com/liveonatlantic/" class="social-list--instagram" target="_blank"></a>
      </li>
      <li class="social-list__item">
        <a href="http://twitter.com/BeachStreetUSA" class="social-list--twitter" target="_blank"></a>
      </li>
    </ul>
  </div>
</div>

<div class="sprinkle-wrapper">

<div id="wrapper">
			<?php if($page['rotatorbelow']): ?>
	<div class="bannerbelows">
    <?php print render($page['rotatorbelow']);?>
    </div>
	    <?php endif; ?>
	<div id="content">

		<div id="happening" class="happening">
			<h2 class="happening__title">What’s Happening in Virginia Beach</h2>

			<div id="hwrap">
				<?php print render($page['whats_happening']); ?>
				<?php print render($page['plan_visit']); ?>
			</div>

			<div class="clearfix"></div>
		</div>

		<?php print render($page['content_home']); ?>


	</div>

	<?php if($page['leaderboard']): ?>
		<div id="leaderboard">
			<?php print render($page['leaderboard']); ?>
		</div>
	<?php endif; ?>

	<?php include('footer.php'); ?>


</div><!-- /Wrapper -->
</div>
<?php include('tag.php'); ?>
