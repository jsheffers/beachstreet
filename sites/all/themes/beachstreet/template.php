<?php

/**
 * @file
 * Contains theme override functions and preprocess functions for the beachstreet theme.
 */

/**
 * Changes the default meta content-type tag to the shorter HTML5 version
 */
function beachstreet_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Changes the search form to use the HTML5 "search" input attribute
 */
function beachstreet_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

function beachstreet_preprocess_page(&$vars) {
  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}

function beachstreet_preprocess_node(&$vars) {
  // Get a list of all the regions for this theme
   foreach (system_region_list($GLOBALS['theme']) as $region_key => $region_name) {

     // Get the content for each region and add it to the $region variable
     if ($blocks = block_get_blocks_by_region($region_key)) {
       $vars['region'][$region_key] = $blocks;
     }
     else {
       $vars['region'][$region_key] = array();
     }
   }
}


/**
 * Uses RDFa attributes if the RDF module is enabled
 * Lifted from Adaptivetheme for D7, full credit to Jeff Burnz
 * ref: http://drupal.org/node/887600
 */
function beachstreet_preprocess_html(&$vars) {
  // if (module_exists('rdf')) {
  //   $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
  //   $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
  //   $vars['rdf']->namespaces = $vars['rdf_namespaces'];
  //   $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  // } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
  // }
}

function beachstreet_date_all_day_label() {
  return t('');
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
/*function beachstreet_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('breadcrumb_display');
  if ($show_breadcrumb == 'yes') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $separator = filter_xss(theme_get_setting('breadcrumb_separator'));
      $trailing_separator = $title = '';

      // Add the title and trailing separator
      if (theme_get_setting('breadcrumb_title')) {
        if ($title = drupal_get_title()) {
          $trailing_separator = $separator;
        }
      }
      // Just add the trailing separator
      elseif (theme_get_setting('breadcrumb_trailing')) {
        $trailing_separator = $separator;
      }

      // Assemble the breadcrumb
      return implode($separator, $breadcrumb) . $trailing_separator . $title;
    }
  }
  // Otherwise, return an empty string.
  return '';
}
*/



