(function ($) {

	Drupal.behaviors.beachStreetFunctions = {
	    attach: function (context, settings) {

        var deadline = $('.field-name-field-countdown-date .date-display-single').text();

        function getTimeRemaining(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor((t / 1000) % 60);
          var minutes = Math.floor((t / 1000 / 60) % 60);
          var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
          var days = Math.floor(t / (1000 * 60 * 60 * 24));
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
          };
        }

        function initializeClock(id, endtime) {
          var clock = document.getElementById(id);
          var daysSpan = clock.querySelector('.days');
          var hoursSpan = clock.querySelector('.hours');
          var minutesSpan = clock.querySelector('.minutes');
          // var secondsSpan = clock.querySelector('.seconds');

          function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            // secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
              clearInterval(timeinterval);
            }
          }

          updateClock();
          var timeinterval = setInterval(updateClock, 1000);
        }

        $('.field-name-field-countdown-date .field-items').append(
          '<div class="c-clock" id="clockdiv"><div class="c-clock__item"><span class="days"></span><div class="c-clock__label">Days</div> </div> <div class="c-clock__item"> <span class="hours"></span> <div class="c-clock__label">Hours</div> </div> <div class="c-clock__item"> <span class="minutes"></span> <div class="c-clock__label">Minutes</div> </div> </div></div>'
        );

        $(function() {
          if ( $('#clockdiv')[0] ) {
            initializeClock('clockdiv', deadline);
          }
        });

				// Adds classes to each menu group
				$('.group').each(function(e){
					$(this).addClass('group'+e);
				});

        $('#rotator .view-content').slick({
          dots: false,
          infinite: true,
          centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          variableWidth: true,
          autoplay: true,
          autoplaySpeed: 3500,
        });

				$('.om-maximenu-content').addClass('short');

				$('.block-menu_block-id-3').hide();

				$('.menu-mlid-11576').hover(function(){
					$('.block-menu_block-id-3').show();
					$('.leaf-events .om-maximenu-content').addClass('wide');
					$('.leaf-events .om-maximenu-content').removeClass('short');
				});

				$('.menu-mlid-11579').hover(function(){
					//$('.block-views-id-menu_event_list-block').show();
					//$('.leaf-events .om-maximenu-content').addClass('wide');
					//$('.leaf-events .om-maximenu-content').removeClass('short');
				});

				$('.block-menu_block-id-2 .content ul li').not('.menu-mlid-11579').hover(function(){
					$('.block-views-id-menu_event_list-block').hide();
					$('.leaf-events .om-maximenu-content').removeClass('wide');
					$('.leaf-events .om-maximenu-content').addClass('short');
				});

				$('.menu-mlid-11575').hover(function(){
					$('.block-views-id-menu_event_list-block').hide();
					$('.block-menu_block-id-2').show();

				});

				$('.block-menu_block-id-1 .content ul li ul li').not('.menu-mlid-11575').hover(function(){
					$('.block-views-id-menu_event_list-block').hide();
					$('.block-menu_block-id-2').hide();
						$('.leaf-events .om-maximenu-content').removeClass('wide');
					$('.leaf-events .om-maximenu-content').addClass('short');
				});

				$('.block-menu_block-id-1 .content ul li ul li').not('.menu-mlid-11576').hover(function(){
					$('.block-views-id-menu_event_list-block').hide();
					$('.block-menu_block-id-3').hide();
					// $('.leaf-events .om-maximenu-content').removeClass('wide');
					// $('.leaf-events .om-maximenu-content').addClass('short');
				});

				$('a.link-events').hover(function(){

					$('.block-views-id-menu_event_list-block').hide();
					$('.block-menu_block-id-2').hide();
						$('.leaf-events .om-maximenu-content').removeClass('wide');
					$('.leaf-events .om-maximenu-content').addClass('short');
			});



				$("#block-views-upcoming-festivals-block .hdates .htime").each(function() {
				    var text = $(this).text();
				    text = text.replace("to", "-");
				    $(this).text(text);
				});

				// Rearrange social icons
				$('.st_fblike_hcount').insertBefore('.st_email_hcount');

				// Text Replacements
				$('#edit-field-date-value-min-wrapper label').text('from');
				$('#edit-field-date-value-max-wrapper label').text('to');

				// Duplicate text field from 1 text field to 2 hidden textfields
				// to run the proper query
				$('#views-exposed-form-calendar-full-page-1 .views-submit-button .form-submit').click(function(){
					var startingDate = $('#edit-sd-min-datepicker-popup-1').val();
					var endingDate = $('#edit-sd-max-datepicker-popup-1').val();

					$('#edit-fw-min-datepicker-popup-1').val(startingDate);
					$('#edit-fw-max-datepicker-popup-1').val(endingDate);


					var startingDate = $('#edit-fd-min-datepicker-popup-0').val();
					var endingDate = $('#edit-fd-max-datepicker-popup-0').val();

					$('#edit-fw-min-datepicker-popup-0').val(startingDate);
					$('#edit-fw-max-datepicker-popup-0').val(endingDate);


				});


				//  Calendar filtering fade ins and outs
				$('#festivals').click(function(event){
					event.preventDefault();
					$(this).toggleClass('clicked');
					$('.festival').fadeToggle(500);
				});

				$('#events').click(function(event){
					event.preventDefault();
					$(this).toggleClass('clicked');
					$('.shows').fadeToggle(500);
				});

				$('#fireworks').click(function(event){
					event.preventDefault();
					$(this).toggleClass('clicked');
					$('.field-fworks').fadeToggle(500);
				});

				$('#towncenter').click(function(event){
					event.preventDefault();
					$(this).toggleClass('clicked');
					$('.towncenter-festivals').fadeToggle(500);
				});


      $('.mobile-toggle').on('click', function() {
        $('.header-menu').toggle();
      });

      $('.om-menu > .om-leaf').each(function() {
        $(this).prepend('<span class="header-arrow"></span>');
      });



      $('.om-menu > .om-leaf > span').on('click', function() {

        if(!$(this).hasClass('is-hidden')) {
          $('.header-arrow').removeClass('is-hidden');
          $('.om-maximenu-content').hide();
          $(this).addClass('is-hidden');
          $(this).next().next().show();
        }
        else {
          $(this).removeClass('is-hidden');
          $(this).next().next().hide();
        }

      });

      // $('.header--mobile .om-menu-ul-wrapper > .om-menu').prepend('<li class="om-leaf"><a href="">Home</a></li>');






				// if( $('body').hasClass('front') ) { // Stops offset() from being run on subpages

				// 	// Adding stripes to h2's
    //       $(window).load(function () {
    //         // run code


				// 	$('.front #wrapper #content .block h2').each(function(e){
				// 		$('#wrapper').before('<div class="stripe' + e + ' stripe" ></div>');
				// 	});

				// 	var head0 = $('.front h2.main').offset().top;
				// 	$('.stripe0').css({  top: head0 + 10  });

				// 	var head1 = $('.front #block-views-upcoming-festivals-block h2').offset().top;
				// 	$('.stripe1').css({  top: head1 + 10  });

				// 	//var head2 = $('.front #block-views-announcement-block-1 h2').offset().top;
				// 	//$('.stripe2').css({  top: head2 + 10  });
				// 	$('.stripe2').css({  height: '0px' });


				// 	var head3 = $('.front #block-views-photos-home-page-block h2').offset().top;
				// 	$('.stripe3').css({  top: head3 + 10  });

    //       });

				// }


				// Moves the happening div inside of the content region to allow for correct positioning
				$('.not-front .region-content').append( $('.not-front #happening') );


				// Wraps the tables in a div. To allow for patterns in IE

				$('.view-calendar-full .views-view-grid').each(function(){
					$(this).wrap('<div class="tables"></div>');
				});


				// Sets height of Sec-rotator div and applys a body class
				// in order to push the sidebar down

				var secHeight = $('#sec-rotator').height();
				if (secHeight == 0) {
					$('body').addClass('push-down');
				} else {
					$('body').addClass('set-height');
				}


				// Adds navigation element to rotator

				// $('#rotator-wrapper').append('<a href="#" id="next">Next</a>');

				// Events for navigation when clicked

				// $('#next').click(function(){
				// 	$temp = $('#rotator').find('.view-content ul li:last').detach();
				//     $temp.prependTo('#rotator .view-content ul');
				// 	$('#rotator').css({ left: -1452  }).animate({ left: -726 });
				// });

				// Resizes the images & keeps them centered
				function resizeHider(){$('#hider').width($(window).width());}
				$(window).resize(function() {
			        resizeHider();
			    });

				// Sets time interval for slideshow

				var link = $('#next');
				timerid = window.setInterval(function() {
				  link.click();
				}, 3000);

				// $('#hider').hover(function(){
				// 	clearInterval(timerid);
				// }, function() {
				// 	var link = $('#next');
				// 	timerid = window.setInterval(function() {
				// 	  link.click();
				// 	}, 3000);
				// });


				/* Scroll Animations */

				$(window).scroll(function(){

					var scrolledPix = $(window).scrollTop();
					$('#ball').rotate(scrolledPix);

					$('.kite.right').css("bottom", -2000 + scrolledPix * 2);
					$('.kite.left').css("bottom", -1700 + scrolledPix);

				});

				$('#sidebar .block:first').addClass('first-block');
				$('#sidebar .block:last').addClass('last-block');
				$('.view-today-on-beach-street-usa .views-row-last').after($('.vmore'));

				$('.pane-node-field-sponsors-field .pane-content').prepend('<h5>Thank You to our sponsors</h5>');
				$('.pane-node-field-sponsors-field .pane-content').append('<a href="/sponsors" id="partner">Be a partner!</a>');



				$("a[href*='http://']:not([href*='"+location.hostname.replace
           			("www.","")+"'])").each(function() {
        				$(this).attr('target', '_blank');
    			});


           		// Fixing date issue for Holiday Lights

           		$('.view-upcoming-festivals .views-row').each(function(){
           			if ( $(this).find('.htitle').html() === "Holiday Lights Maola Milk Merry Mile" ) {
           				$(this)	.addClass('specialdate');
           				$(this).find('.hmonth').html("Nov 23");
           				$(this).find('.htime').html("- Dec 31");
           			};
           		$('')
           		});
           $("#furdd-furdd").attr("placeholder", "YOUR EMAIL");

           		$('#block-views-exp-calendar-full-page-1 .content').prepend("<p class='plan-your-visit-text'>Set up your summer fun in Virginia Beach and have something to look forward to for the coming months. And once your plans are set, you can just sit back, relax, and enjoy BeachStreetUSA.</p>")


	    }
	  }

})(jQuery);


