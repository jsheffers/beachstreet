<?php


?>
<div class="header--mobile">
  <div class="header-banner">
    <a href="#" class="mobile-toggle">Menu</a>
  </div>
  <div class="header-menu">
    <?php print render($page['nav']);?>
  </div>
</div>


<div id="header"><div class="section">

	<?php print render($page['topheader']);?>
	<div id="menu">
		<?php print render($page['nav']);?>
	</div>
	<div class="clearfix"></div>

	<div id="mega"></div>

</div></div><!-- /Section & /Header -->

<div class="sprinkle-wrapper">
<div id="wrapper">

	<div class="landing-content">

		<div id="">
      <?php print $messages; ?>
      <?php print render($title_prefix); ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?>
        <div class="tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

			<?php print render($page['content']); ?>
		</div><!-- /LEFT -->

	</div>

	<?php include('footer.php'); ?>

</div><!-- /Wrapper -->
</div>
<?php include('tag.php'); ?>

