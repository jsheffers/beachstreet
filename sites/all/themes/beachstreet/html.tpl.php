<?php



/**

 * @file

 * Default theme implementation to display the basic html structure of a single

 * Drupal page.

 *

 * Variables:

 * - $css: An array of CSS files for the current page.

 * - $language: (object) The language the site is being displayed in.

 *   $language->language contains its textual representation.

 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.

 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.

 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.

 * - $head_title: A modified version of the page title, for use in the TITLE tag.

 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and

 *   so on).

 * - $styles: Style tags necessary to import all CSS files for the page.

 * - $scripts: Script tags necessary to load the JavaScript files and settings

 *   for the page.

 * - $page_top: Initial markup from any modules that have altered the

 *   page. This variable should always be output first, before all other dynamic

 *   content.

 * - $page: The rendered page content.

 * - $page_bottom: Final closing markup from any modules that have altered the

 *   page. This variable should always be output last, after all other dynamic

 *   content.

 * - $classes String of classes that can be used to style contextually through

 *   CSS.

 *

 * @see template_preprocess()

 * @see template_preprocess_html()

 * @see template_process()

 */

?><?php print $doctype; ?>

<html>

<head>


  <?php print $head; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php print $head_title; ?></title>

  <?php print $styles; ?>

  <?php print $scripts; ?>

  <script type='text/javascript'>



	</script>


<style>

.block-menu_block ul.menu{
	list-style:none;
}
.block-menu_block ul.menu li {
    color: #FFFFFF;
    list-style: disc outside none;
    margin: 0;
    padding: 0;
}
.block-menu_block ul.menu li ul{
	list-style:none;

    margin: 0;
    padding: 0;
}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right .block-menu_block-id-1{
    /* width: 150px; */

    background: none !important;
}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right{
	max-height:330px;
}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right .block-menu_block-id-2{
    background: none repeat scroll 0 0 #DB3434;

    color: #FFFFFF;
    display: block;
    left: 160px;
    padding-left: 0px;
    position: absolute;
    width: 168px !important;
	top:0px;

}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right .block-menu_block-id-3{
    background: none repeat scroll 0 0 #DB3434;

    color: #FFFFFF;
    display: block;
    left: 160px;
    padding-left: 0px;
    position: absolute;
    width: 168px !important;
  top:0px;

}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right .block-menu_block-id-2 .content{
    background: url("/sites/all/themes/beachstreet/images/mshadow.png") no-repeat scroll left top rgba(0, 0, 0, 0);
    float: left;
    margin-top: -10px;
    padding: 10px 7px 4px 22px;
	min-height:282px;
	margin-bottom:-16px;
}
.leaf-events .om-maximenu-middle .om-maximenu-middle-right .block-menu_block-id-3 .content{
    background: url("/sites/all/themes/beachstreet/images/mshadow.png") no-repeat scroll left top rgba(0, 0, 0, 0);
    float: left;
    margin-top: -10px;
    padding: 10px 7px 4px 22px;
  min-height:282px;
  margin-bottom:-16px;
}


.block-views-id-menu_event_list-block {
    left: 184px;

}
.block-views-id-menu_event_list-block .group{
	width:187px;
	background: url("/sites/all/themes/beachstreet/images/mshadow.png") no-repeat scroll left top rgba(0, 0, 0, 0) !important;
 padding: 10px 10px 10px 20px;
}

</style>



</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>



  <?php print $page_top; ?>

  <div style='position: absolute;left: -1898px;top: -2001px;'>

  </div>

  <?php print $page; ?>

  <?php print $page_bottom; ?>



</body>

</html>
