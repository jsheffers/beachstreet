<?php


?>
<div class="header--mobile">
  <div class="header-banner">
    <a href="#" class="mobile-toggle">Menu</a>
  </div>
  <div class="header-menu">
    <?php print render($page['nav']);?>
  </div>
</div>


<div id="header"><div class="section">

	<?php print render($page['topheader']);?>
	<div id="menu">
		<?php print render($page['nav']);?>
	</div>
	<div class="clearfix"></div>

	<div id="mega"></div>

</div></div><!-- /Section & /Header -->

<div class="sprinkle-wrapper">


<div id="wrapper">

	<div id="sec-rotator">

		<?php if ($logo): ?>
		  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
		    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
		  </a>
		<?php endif; ?>

		<?php print render($page['sec_rotator']); ?>
	</div>

	<div id="content">

		<div id="left">

			<!-- <?php if($page['plan_visit']): ?>
			<div id="happening">
				<div id="hwrap">
					<?php print render($page['whats_happening']); ?>
					<?php print render($page['plan_visit']); ?>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php endif; ?>	 -->

			 <?php if ($breadcrumb): ?>
		        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
		      <?php endif; ?>
		      <?php print $messages; ?>
		      <?php print render($title_prefix); ?>
		      <?php if ($title): ?>
		        <h1 class="title" id="page-title"><?php print $title; ?></h1>
		      <?php endif; ?>
		      <?php print render($title_suffix); ?>
		      <?php if ($tabs): ?>
		        <div class="tabs"><?php print render($tabs); ?></div>
		      <?php endif; ?>
		      <?php print render($page['help']); ?>
		      <?php if ($action_links): ?>
		        <ul class="action-links"><?php print render($action_links); ?></ul>
		      <?php endif; ?>


			<?php print render($page['content']); ?>

		</div><!-- /LEFT -->

		<div id="sidebar">
			<?php print render($page['sidebar_first']); ?>
		</div>

	</div>

	<?php if($page['leaderboard']): ?>
		<div id="leaderboard">
			<?php print render($page['leaderboard']); ?>
		</div>
	<?php endif; ?>


	<?php include('footer.php'); ?>

</div><!-- /Wrapper -->
</div>
<?php include('tag.php'); ?>

