var gulp = require('gulp');

// Access plugins like this: $.autoprefixer()
var $ = require('gulp-load-plugins')({camelize: true});

var onError = function(err) {
  $.notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Error: <%= error.message %>",
    sound:    "Beep"
  })(err);
  this.emit('end');
};

gulp.task('css', function() {
  gulp.src('style.scss')
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.sass({outputStyle: 'expanded', errLogToConsole: true, sourceComments: true }))
    .pipe($.autoprefixer())
    .pipe($.size({showFiles: true}))
    .pipe(gulp.dest('./'))
    .pipe($.notify({
      onLast: true,
      message: function () {
        return 'Sass compiled :)';
      }
    }));
});

gulp.task('watch', function() {
  gulp.watch('./**/*.scss', ['css']);
});

// Default dev task.
gulp.task('default', ['css', 'watch']);