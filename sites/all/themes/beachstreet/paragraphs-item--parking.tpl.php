<div class="<?php print $classes; ?>"<?php print $attributes; ?> id="parking">
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>
</div>
